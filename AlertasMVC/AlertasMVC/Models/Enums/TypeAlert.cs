﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlertasMVC.Models.Enums
{
    public  class TypeAlert
    {
        public const string KuramaExito = "success";
        public const string KuramaError = "danger";
        public const string KuramaPeligro = "warning";
        public const string KuramaInfo = "info";

        public static string[] KuramaAlertas
        {
            get { return new[] { KuramaExito, KuramaError, KuramaInfo, KuramaPeligro }; } 
        }
    }
}
