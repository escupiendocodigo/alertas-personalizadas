﻿using AlertasMVC.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace AlertasMVC.Controllers
{
    public class HomeController : AlertasBootstrapController
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            KuramaExito("Hola bienvenido al index");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult ALertas()
        {
            KuramaExito("Exito");
            KuramaError("Error");
            KuramaInfo("Info");
            KuramaPeligro("Peligro");
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
