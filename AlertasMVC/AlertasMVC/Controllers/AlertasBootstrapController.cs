﻿using AlertasMVC.Models.Enums;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlertasMVC.Controllers
{
    public class AlertasBootstrapController : Controller
    {
        public void KuramaExito(string mensaje)
        {
            
            TempData.Add(TypeAlert.KuramaExito, mensaje);
        }
        public void KuramaError(string mensaje)
        {
            TempData.Add(TypeAlert.KuramaError, mensaje);
        }
        public void KuramaPeligro(string mensaje)
        {
            TempData.Add(TypeAlert.KuramaPeligro, mensaje);
        }
        public void KuramaInfo(string mensaje)
        {
            TempData.Add(TypeAlert.KuramaInfo, mensaje);
        }
    }
}
